import collections
from distutils.log import error
from flask import Flask, render_template, request, url_for, redirect
from http import client
from pymongo import MongoClient
import os

client= MongoClient("mongodb+srv://Gerard:scpporadnik@cluster0.476pn.mongodb.net/scp_tutorial?retryWrites=true&w=majority")
db= client.get_database('scp_tutorial')
Postacie = db.Postacie
Logowanie = db.System_Logowania

def Date(id):
    data = Postacie.find({"_id":id})
    for i in data:
        Date.types = i['klasa']
        Date.name = i['nazwa']
        if 'numerSCP' in i:
            Date.number = i['numerSCP']
        Date.dire = i['opis']

def Register(Email, Login, Password):

    id_number = Logowanie.count_documents({}) + 1
    Logowanie.insert_one({"_id":id_number, "Email":Email, "Login":Login, "Hasło":Password})

def Login():
    for i in range(Logowanie.count_documents({})+1):
        login = list(Logowanie.find({"_id":i}))
        for j in login:
            Login.email = j['Email']
            Login.username = j['Login']
            Login.password = j['Hasło']
            if request.method == 'POST':
                if request.form['username'] == Login.username and request.form['password'] == Login.password:
                    Login.yes = True
                    return
                else:
                    Login.yes = False


app=Flask(__name__)

Nazwa = 0
Klasa = 0
Numer = 0
Opis = 0
user_plus = 0

@app.route('/')
def main():
    return render_template('index.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    Register(request.form['email'], request.form['username'], request.form['password'])
    return redirect(request.referrer)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        Login()
        if Login.yes == True:
            global user_plus
            user_plus = 'Tak gg'
        else:
            return redirect(request.referrer)
    return redirect(request.referrer)


@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        return redirect(url_for('index'))

    return render_template('index.html', error = user_plus)

@app.route('/klasad', methods=['GET', 'POST'])
def klasad():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(8)
    return render_template('klasad.html', Nazwa = Date.name, Klasa = Date.types, Opis = Date.dire, error = user_plus)

@app.route('/naukowiec', methods=['GET', 'POST'])
def naukowiec():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(9)
    return render_template('naukowiec.html', Nazwa = Date.name, Klasa = Date.types, Opis = Date.dire, error = user_plus)

@app.route('/ci', methods=['GET', 'POST'])
def ci():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(10)
    return render_template('ci.html', Nazwa = Date.name, Klasa = Date.types, Opis = Date.dire, error = user_plus)

@app.route('/jakzaczac', methods=['GET', 'POST'])
def jakzaczac():
    if request.method == 'POST':
        return redirect(url_for('index'))
        
    return render_template('jakzaczac.html', error = user_plus)

@app.route('/mtf', methods=['GET', 'POST'])
def mtf():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(11)
    return render_template('mtf.html', Nazwa = Date.name, Klasa = Date.types, Opis = Date.dire, error = user_plus)

@app.route('/scp049', methods=['GET', 'POST'])
def scp049():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(1)
    return render_template('scp049.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error = user_plus)

@app.route('/scp049-2', methods=['GET', 'POST'])
def scp0492():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(2)
    return render_template('scp049-2.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)

@app.route('/scp079', methods=['GET', 'POST'])
def scp079():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(3)
    return render_template('scp079.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)

@app.route('/scp096', methods=['GET', 'POST'])
def scp096():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(4)
    return render_template('scp096.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)


@app.route('/scp106', methods=['GET', 'POST'])
def scp106():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(5)
    return render_template('scp106.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)

@app.route('/scp173', methods=['GET', 'POST'])
def scp173():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(6)
    return render_template('scp173.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)

@app.route('/scp939', methods=['GET', 'POST'])
def scp939():
    if request.method == 'POST':
        return redirect(url_for('index'))

    Date(7)
    return render_template('scp939.html', Nazwa = Date.name, Klasa = Date.types, Numer = Date.number, Opis = Date.dire, error= user_plus)

@app.route('/scp/<numer>')
def scp(numer):
    return f'scp-{numer}'

if __name__=='__main__':
    app.run(debug=True)
